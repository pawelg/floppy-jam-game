import pygame, sys
from pygame.locals import *
from random import shuffle, randint
'''
wall pattern
Height - 10, Font size - 30 
    ##   ## <- row class, row have two wariants - empty(EE) and full(##) 
    ##   ##
    ##   EE
    EE   EE
@   EE   ##
    ##   ##
    ##   ##
    ##   ##
    ##   ##
    ##   ##

    newwall = [Row(full), Row(full), Row(empty), Row(empty), Row(full), Row(full)...Row(full)]
'''
FULL = pygame.image.load('images/row.png')
EMPTY = " "
STARTPOSITION = (200, 150)
WHITE = (255, 255, 255)

class Player:
    def __init__(self, number, board): #number is between 0 to 3
        self.number = number

        self.surf_obj = pygame.image.load('images/player.png')
        self.player_obj = self.surf_obj.get_rect()
        self.x, self.y = STARTPOSITION
        self.x -= number*40 #if is only one player, his position is (300, 150),
        #if is more(two, three or four) next player is shifted 40 pixels  to left
        self.player_obj.topleft=(self.x,self.y)
        board.surf.blit(self.surf_obj, self.player_obj)

        self.jumpSPEED = 0.90
        self.fallSPEED = 0.50
        self.STATE = "STILL"
        self.jumphigh = 0
        self.maxHIGH = 35# we assume that game have 30, 35fps, so jump lasts near 2 seconds

    def move(self, board, number_of_killed):
        if self.STATE == "JUMP":
            self.y -= self.jumpSPEED
            if self.jumphigh > self.maxHIGH:
                self.STATE = "FALL"
                self.jumphigh = 0
            self.jumphigh += self.jumpSPEED
        elif self.STATE == "FALL":
            self.y += self.fallSPEED
        board.surf.blit(self.surf_obj, (self.x, self.y))

class Wall():
    def __init__(self, isempty, gameboard):
        self.x = 599
        self.surf_obj = []
        self.wall_obj = []
        self.y = []
        self.distance_between_rows = 30
        self.random_wall_patterns = [ [FULL, EMPTY, FULL, FULL, FULL], [FULL, FULL, EMPTY, FULL, FULL], [FULL, FULL, FULL, EMPTY, FULL], [FULL, FULL, FULL, FULL, EMPTY], [FULL, EMPTY, EMPTY, FULL, FULL], [FULL, FULL, EMPTY, EMPTY, FULL], [FULL, EMPTY, EMPTY, EMPTY, FULL]]
        if isempty == EMPTY:
            self.create_empty_wall(gameboard)
        elif isempty == FULL:
            self.create_random_wall(gameboard)
        self.STATE = isempty
   
    def create_empty_wall(self, gameboard):
        self.pattern = [FULL, FULL, EMPTY, EMPTY, EMPTY]
        self.create_wall(gameboard)

    def create_random_wall(self, gameboard):
        shuffle(self.random_wall_patterns)
        random_number = randint(0, len(self.random_wall_patterns)-1)
        self.pattern = self.random_wall_patterns[random_number]
        self.create_wall(gameboard)
   
    def create_wall(self, gameboard):
        y = 0
        for i in self.pattern:
            if i == FULL:
                self.surf_obj.append(FULL)
                self.wall_obj.append(self.surf_obj[-1].get_rect())
                self.wall_obj[-1].topleft = (self.x,y)
                self.y.append(y)
                gameboard.surf.blit(self.surf_obj[-1], self.wall_obj[-1])
            y+=self.distance_between_rows
    def move_wall(self, gameboard):#one frame
        for i, j in enumerate(self.surf_obj):
                self.x -= gameboard.speed
                gameboard.surf.blit(self.surf_obj[i],(self.x, self.y[i]))

    def iscollide(self, player):
        return False


class Board(pygame.sprite.Sprite):
    def __init__(self, name):
        self.STATE = 'START'
        self.speed = 0.3
        self.distance_between_full_walls = 3
        self.surf = pygame.display.set_mode((600, 300), 0, 32)
        pygame.display.set_caption(name)
        self.walls = []

        self.load_background_image()
        self.width_background_image = 300

        self.width_wall = 45
        self.create_wall(EMPTY)
        self.create_menu_text()

        self.menu(1)

    def create_menu_text(self):
        self.menu_text = []
        self.font_obj = pygame.font.Font('zig.ttf', 30)
        self.menu_text.append(self.font_obj.render('Press UP to start', True, WHITE))
        self.menu_text.append(self.font_obj.render('Players', True, WHITE))

    def menu(self, number_of_players):
        self.STATE = 'MENU'
        self.board_animation()
        x, y = 100, 100
        self.surf.blit(self.menu_text[0], (x, y)) 
        text = self.font_obj.render('<< '+str(number_of_players)+' >>', True, WHITE)
        self.surf.blit(text, (230, y+40))
        self.surf.blit(self.menu_text[1], (x+130, y+70))
    def play(self):
        self.STATE = 'PLAY'
        self.board_animation()

    def board_animation(self):
        self.move_background_image()
        self.move_walls()
        if self.walls[-1].x <=560 and self.walls[-1].x > 559:# when last wall is show almost full, new wall can be created
            if self.STATE == 'MENU' or self.walls[self.distance_between_full_walls].STATE == EMPTY: 
                self.create_wall(EMPTY)
            elif self.STATE == 'PLAY' and self.walls[self.distans_between_full_walls].STATE == FULL:
                self.create_wall(FULL)

    def move_walls(self):
        for i, j in enumerate(self.walls):
            self.walls[i].move_wall(self)

        if self.walls[0].x <-self.width_wall:
            del self.walls[0]
            self.create_wall(EMPTY)

    def create_wall(self, isempty):
        self.walls.append(Wall(isempty, self))

    def load_background_image(self):
        self.background_image = pygame.image.load("images/backgroundimage.png")
        self.background_rect = self.background_image.get_rect()
        self.board_x, self.board_y = (0,0)
        self.background_rect.topleft = (self.board_x, self.board_y)
        self.surf.blit(self.background_image, self.background_rect)

    def move_background_image(self):
        if self.board_x > -(self.width_background_image/2):
            self.board_x -= self.speed
        else:
            self.board_x = 0
        self.surf.blit(self.background_image, (self.board_x, self.board_y))


class Game:
    def __init__(self):
        pygame.init()
        self.players = []
        self.number_of_players = 1
        self.gameboard = Board('flappy jam game')
        self.update()
    def run(self):
        for i in range(self.number_of_players):
            self.players.append(Player(i, self.gameboard))
        self.update()
    def update(self):
        number_of_killed = 0
        while True:
            for event in pygame.event.get():
                if event.type == QUIT:
                    print("wychodzimy z programu")
                    pygame.quit()
                    sys.exit()
                    break
                if event.type == KEYDOWN and self.gameboard.STATE == 'PLAY':
                    if event.key == K_UP:
                        self.players[0].STATE = "JUMP"
                    elif event.key == K_p:
                        self.players[0].STATE = "STILL"
                if event.type == KEYDOWN and self.gameboard.STATE == 'MENU':
                    if event.key == K_UP:
                        self.gameboard.STATE = 'PLAY'
                        self.run()
                    elif event.key == K_LEFT:
                        if self.number_of_players > 1:
                            self.number_of_players -=1
                    elif event.key == K_RIGHT:
                        if self.number_of_players < 4:
                            self.number_of_players +=1

            if self.gameboard.STATE == 'MENU':
                self.gameboard.menu(self.number_of_players)
            elif self.gameboard.STATE == 'PLAY':
                self.gameboard.play()

            for player in self.players:
                player.move(self.gameboard, number_of_killed)
#            if number_of_killed == len(self.players):
#                self.gameboard.STATE = 'MENU'
            pygame.display.update()

